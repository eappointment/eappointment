# Software architecture and infrastructure considerations

The software uses a service oriented architecture. Every available frontend is browser based. On the side of a browser client, a typical request flow would look like this:

```mermaid
graph LR

browser["Browser Client"]
frontend("Frontend application")
api("HTTP API")
db[("MySQL Database")]

browser --"HTTP Request"--> frontend
frontend --"HTTP Request"--> api
api --"SQL Query"--> db
db --"Data"--> api
api--"JSON"-->frontend
frontend--"HTML"-->browser

```

In some special cases, a quota for API calls is required and for security reasons, the access to the backend API is restricted. In this case, there is an API Proxy to ensure restricted access and usage quotas:


```mermaid
graph LR

browser["Browser Client"]
frontend("Frontend application")
proxy("API Proxy")
api("HTTP API")
db[("MySQL Database")]

browser --"HTTP Request"--> frontend
frontend --"HTTP Request"--> proxy
proxy --"Checks quotas"--> api
api --"SQL Query"--> db
db --"Data"--> api
api --"JSON"--> proxy
proxy--"JSON"-->frontend
frontend--"HTML"-->browser

```

The only part to store data is the MySQL database. Any other component is stateless. This ensures horizontal scalability of all components except the database. To allow scalability of the database, the Galera-Cluster solution is supported. The are different possible infrastructure solutions, to host the application. A simple solution might look like this:

```mermaid
flowchart TB

browser["Browser Client"]

subgraph Loadblanced-Frontend-Cluster
direction TB
frontend1("Frontend application")
frontend2("Frontend application")
end
subgraph Loadblanced-API-Cluster
direction TB
api1("HTTP API")
api2("HTTP API")
end
subgraph Loadblanced-DB-Cluster
direction TB
db1[("MySQL Node 1")]
db2[("MySQL Node 2")]
db3[("MySQL Node 3")]
end

browser --> Loadblanced-Frontend-Cluster
Loadblanced-Frontend-Cluster --> Loadblanced-API-Cluster
Loadblanced-API-Cluster --> Loadblanced-DB-Cluster

```

A frontend application an the HTTP API can run on the same webserver. It is strongly recommended to **avoid sharing the same request pool between frontend application and API**. This might lead to a situation where a frontend request might not run, because it can not get a free HTTP slot for an API request. To avoid deadlocks of this kind, you should ensure, that the API has a exclusive pool for HTTP requests.

For a database cluster, it is recommended to declare a single read write node as master for write requests. The application does not support sharding (yet), so every write requests has to happen on every database node. A single node for write requests avoids deadlocks due to racing conditions. A setup might look like this:

```mermaid
flowchart TB

subgraph Loadblanced-API-Cluster
direction TB
api1("HTTP API")
api2("HTTP API")
end
subgraph Loadblanced-DB-Cluster
direction TB
db1[("MySQL Node 1")]
db2[("MySQL Node 2")]
db3[("MySQL Node 3")]
end


api1 --RW-->db1
api1 --RO-->db2
api2 --RW-->db1
api2 --RO-->db3

```

## Software Dependencies

There are some central libraries to consider on planning new features. The most fundamental library is [zmsentities](https://gitlab.com/eappointment/zmsentities). In the [subdirectory "schema"](https://gitlab.com/eappointment/zmsentities/-/tree/main/schema) are the definitions for the entities, which are the base for all operations in the application.

The library [zmsdb](https://gitlab.com/eappointment/zmsdb) is used for database operations and is responsible for persisting every bit of data. For every entity in `zmsentities` there is a class for storing and fetching data for this entity.

Since we habe a service oriented architecture, every access to fetch or persist data happens through the [zmsapi](https://gitlab.com/eappointment/zmsapi). The [API documentation](https://eappointment.gitlab.io/zmsapi/doc/) has all available routes to cread, read, update or delete data. The routes are grouped by entities matching the entities from `zmsentities`.

To access `zmsapi` you can use the library [zmsclient](https://gitlab.com/eappointment/zmsclient). Since per definition, a Galera database cluster might encounter deadlocks, this library ensures to repeat requests, if a deadlock exception is detected. It additionally sets and handles cookies, if a workstation login happens.

For handling HTTP requests, the application uses the micro framework [PHP Slim](https://www.slimframework.com/). The library [zmsslim](https://gitlab.com/eappointment/zmsslim) contains shared functionality between application components. It handles bootstraping, common middlewares and a few sensible default settings.

To check input parameters, the application uses [mellon](https://gitlab.com/eappointment/mellon). This library validates input and filters malicious data.

Additionally the application uses further libraries, like [PHPUnit](https://phpunit.de/) for testing or [PHPMD](https://phpmd.org/) and [PHP Codesniffer](https://github.com/squizlabs/PHP_CodeSniffer) to ensure code quality. For further coding guidelines, refer to [CONTRIBUTING.md](../CONTRIBUTION.md).

The following diagram show the relations between applications and libraries:

```mermaid
graph LR




zmsmanual

zmsapi

mysql[("Database")]

subgraph Applications
zmsapiproxy
zmsmessaging
zmsadmin
zmsstatistic
zmscalldisplay
zmswebcalldisplay
zmsticketprinter
d115mandant
end

subgraph "External Applications"
zmsappointment("Booking Frontend in a portal")
chatbot
mobileapp
qtv("QTV Qualifizierte TerminVereinbarungen")
end

subgraph Libraries
mellon
zmsentities
zmsclient
zmsdb
zmsslim
zmsdldb
layout-admin-scss
layout-admin-js
end

zmsadmin -.uses.-> zmsentities
zmsappointment -.uses.-> zmsentities
zmsapiproxy -.uses.-> zmsentities
zmsmessaging -.uses.-> zmsentities
zmsstatistic -.uses.-> zmsentities
zmscalldisplay -.uses.-> zmsentities
zmswebcalldisplay -.uses.-> zmsentities
zmsticketprinter -.uses.-> zmsentities
d115mandant -.uses.-> zmsentities
zmsapi -.uses..-> zmsentities

zmsadmin -.uses.-> zmsslim
zmsappointment -.uses.-> zmsslim
zmsapiproxy -.uses.-> zmsslim
zmsstatistic -.uses.-> zmsslim
zmscalldisplay -.uses.-> zmsslim
zmswebcalldisplay -.uses.-> zmsslim
zmsticketprinter -.uses.-> zmsslim
d115mandant -.uses.-> zmsslim
zmsapi -.uses.-> zmsslim

zmsslim -.uses.-> mellon

zmsadmin -.uses.-> layout-admin-scss
zmsadmin -.uses.-> layout-admin-js


zmsadmin -.uses.-> zmsclient
zmsappointment -.uses.-> zmsclient
zmsapiproxy -.uses.-> zmsclient
zmsmessaging -.uses.-> zmsclient
zmsstatistic -.uses.-> zmsclient
zmscalldisplay -.uses.-> zmsclient
zmswebcalldisplay -.uses.-> zmsclient
zmsticketprinter -.uses.-> zmsclient
d115mandant -.uses.-> zmsclient


zmsapi -.uses..-> zmsdb

zmsapi -.uses.-> zmsdldb
zmsappointment -.uses.-> zmsdldb

zmsadmin -.links....-> zmsmanual

chatbot --calls--> zmsapiproxy
mobileapp --calls--> zmsapiproxy
qtv --calls--> zmsapiproxy

zmsapiproxy --calls--> zmsapi
zmsappointment --calls--> zmsapi
zmsmessaging --calls--> zmsapi
zmsadmin --calls--> zmsapi
zmsstatistic --calls--> zmsapi
zmscalldisplay --calls--> zmsapi
zmswebcalldisplay --calls--> zmsapi
zmsticketprinter --calls--> zmsapi
d115mandant --calls--> zmsapi

zmsdb ==access==> mysql

```