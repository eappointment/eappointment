The detailed listing of available appointment slots is calculated by zmsapi::ProcessFree \
The detailed listing of available appointment slots is requested from 2 components \ 3 places:

```mermaid
sequenceDiagram
    actor Bürger
    Bürger->>+zmsappointment: request TimeSelect(selecteddate, ..)
    Note over zmsappointment: with session data by Cookie 
    zmsappointment->>zmsappointment: build CalendarData
    zmsappointment->>+zmsapi: request ProcessFree(CalendarData)
    zmsapi-->>-zmsappointment: processList
    zmsappointment-->>-Bürger: free Appointments
    
```
```mermaid
sequenceDiagram
    actor Sachbearbeiter
    Sachbearbeiter->>+zmsadmin: request AppointmentFormFreeProcessList(..)
    zmsadmin->>+zmsapi: request ProcessFree(..)
    zmsapi-->>-zmsadmin: processList
    zmsadmin-->>-Sachbearbeiter: free Appointments
```
```mermaid
sequenceDiagram
    actor Sachbearbeiter
    Sachbearbeiter->>+zmsadmin: request CalendarWeek(..)
    zmsadmin->>zmsadmin: build CalendarData
    zmsadmin->>+zmsapi: request ProcessFree(CalendarData)
    zmsapi-->>-zmsadmin: processList
    zmsadmin-->>-Sachbearbeiter: free Appointments
```

The calculation of available appointments with the requested 'slotsRequired' needs a temporary database table\
that stores the slotsRequired for the selected scope(s).\
**The required slots** (slotsRequired) are calculated by using the service data of the requested services with the appointment,\
**or** it is precalculated with javascript of the admin-appointment form\
( js\block\appointment\requests.js: slotCount = Math.ceil(requestList.reduce(..)) )

```mermaid
sequenceDiagram
    note over zmsapi\ProcessFree: Parameter: calendar, slotType, slotsRequired, ..
    zmsapi\ProcessFree->>zmsapi\ProcessFree: resolve Calendar Entity including slotsRequired per scope
    zmsapi\ProcessFree->>+zmsdb\ProcessStatusFree: readFreeProcesses(calendar, slotType, slotsRequired)
    zmsdb\ProcessStatusFree->>zmsdb\ProcessStatusFree: writeTemporaryScopeList(slotsRequired, ..)
    Note right of zmsdb\ProcessStatusFree: the list contains the slotsRequired<br> for each scope, year and month<br>where slotsRequired = slotsRequired ||<br> roundToInt(calender->scopes->getRequiredSlotsByScope(..))
    zmsdb\ProcessStatusFree->>zmsdb\ProcessStatusFree: fetch and create processList 
    zmsdb\ProcessStatusFree-->>-zmsapi\ProcessFree: processList
```
where the Calendar Entity calculates the slotsRequired when resolving the requests where each request has a slot-factor/slot-count related to the current scope (request-provider-relation) that can be a float number:
```
pseudoCode:

slotsRequired := 0
foreach (request) do:
    requestProviderRelation := getRelationByRequest(request)
    slotsRequired := slotsRequired + requestProviderRelation->getSlotCount()
end foreach

assign slotsRequired to the calendarData
```

So the temporary table for the current process request looks like:\
(sample data was cut to relevant info and is showing multiple selected scopes)

| scopeId | year | month | slotsRequired |
|---------|------|-------|---------------|
| 345     | 3210 | 12    | 2.4           |
| 456     | 3210 | 12    | 3             |
| 141     | 3210 | 12    | 2             |
| ..      | ..   | ..    | ..            |

So for one scope it will just be one line:

| scopeId | year | month | slotsRequired |
|---------|------|-------|---------------|
| 141     | 2024 | 3     | 2             |

All slots of an availability time are precalculated / updated and look like\
(worker represents the manpower of the requested slot type)

| slotID | scopeID | year | month | day | time     | worker | status | slotTimeInMinutes |
|--------|---------|------|-------|-----|----------|--------|--------|-------------------|
| 62980  | 141     | 2024 | 3     | 15  | 08:00:00 | 2      | free   | 20                |
| 62981  | 141     | 2024 | 3     | 15  | 08:20:00 | 2      | free   | 20                |
| 62982  | 141     | 2024 | 3     | 15  | 08:40:00 | 2      | free   | 20                |
| 62983  | 141     | 2024 | 3     | 15  | 09:00:00 | 1      | free   | 20                |
| 62984  | 141     | 2024 | 3     | 15  | 09:20:00 | 1      | free   | 20                |
| ..     | ..      | ..   | ..    | ..  | ..       | ..     | ..     | ..                |
| 67531  | 141     | 2024 | 4     | 01  | 08:00:00 | 2      | free   | 20                |
| ..     | ..      | ..   | ..    | ..  | ..       | ..     | ..     | ..                |
| 73645  | 456     | 2024 | 3     | 15  | 08:00:00 | 4      | free   | 10                |
| ..     | ..      | ..   | ..    | ..  | ..       | ..     | ..     | ..                |


(While status is not free only when the office has canceled an availability time,
and worker is the number of people working in parallel)

Another precalculated table (slot_hiera) contains the slot hierarchy 
which is used to store the connection of slots by listing all ancestor slots of each slot.\
Example for 1 of the slots above:

| slotID | ancestorID | ancestorLevel |
|--------|------------|---------------|
| 62982  | 62980      | 3             |
| 62982  | 62981      | 2             |
| 62982  | 62982      | 1             |
So the slot 62980 can have 2 other slots connected to one appointment (in this short example)

To know which of these slots is already booked, the table slot_process is used\
and stores the slotID-processID relation

| slotID | processID |
|--------|-----------|
| 62980  | 162980    |
| 62980  | 262980    |
| 62981  | 876543    |
| 62982  | 657483    |
| 62984  | 293847    |
| ..     | ..        |

### fetch and create process list

The "calculation" of the available appointments with the requiredSlots happens by matching and grouping in a db query.\
( BO\Zmsdb\Query\ProcessStatusFree::QUERY_SELECT_PROCESSLIST_DAY is one of them )\
The selection explained on the data for :

1. **joining the related data (for the 2024-3-15 sample data):**

at first, all slots of 2024-3-15 are matched with the temporary table for the required slot amount,\
so we reduce the slot data to :

| slotID | scopeID | year | month | day | time     | worker | status | slotTimeInMinutes | slotsRequired |
|--------|---------|------|-------|-----|----------|--------|--------|-------------------|---------------|
| 62980  | 141     | 2024 | 3     | 15  | 08:00:00 | 2      | free   | 20                | 2             |
| 62981  | 141     | 2024 | 3     | 15  | 08:20:00 | 2      | free   | 20                | 2             |
| 62982  | 141     | 2024 | 3     | 15  | 08:40:00 | 2      | free   | 20                | 2             |
| 62983  | 141     | 2024 | 3     | 15  | 09:00:00 | 1      | free   | 20                | 2             |
| 62984  | 141     | 2024 | 3     | 15  | 09:20:00 | 1      | free   | 20                | 2             |

Let's ignore the date and scope and focus on the relevant data:  

| slotID | time     | worker | slotsRequired |
|--------|----------|--------|---------------|
| 62980  | 08:00:00 | 2      | 2             |
| 62981  | 08:20:00 | 2      | 2             |
| 62982  | 08:40:00 | 2      | 2             |
| 62983  | 09:00:00 | 1      | 2             |
| 62984  | 09:20:00 | 1      | 2             |

This data is then extended with the ancestor- and the process-relation,\
so that we get the amount of **confirmed** appointments for each slot\
and the sum of slots appended to each slot that has less confirmed appointments than worker\
(we name it **amount**)

| slotID | time     | worker | slotsRequired | amount | confirmed |
|--------|----------|--------|---------------|--------|-----------|
| 62980  | 08:00:00 | 2      | 2             | 4      | 2         |
| 62981  | 08:20:00 | 2      | 2             | 3      | 1         |
| 62982  | 08:40:00 | 2      | 2             | 2      | 1         |
| 62983  | 09:00:00 | 1      | 2             | 1      | 0         |
| 62984  | 09:20:00 | 1      | 2             | 0      | 1         |

2. **filter the data**

The data is then filtered for the amount being enough for the required slots and\
the confirmed appointments being less then available workers/manpower.

| slotID | time     | worker | slotsRequired | amount | confirmed | remove reason                             |
|--------|----------|--------|---------------|--------|-----------|-------------------------------------------|
| 62980  | 08:00:00 | 2      | 2             | 4      | 2         | 2 confirmed is not less than 2 worker     |
| 62981  | 08:20:00 | 2      | 2             | 3      | 1         |                                           |
| 62982  | 08:40:00 | 2      | 2             | 2      | 1         |                                           |
| 62983  | 09:00:00 | 1      | 2             | 1      | 0         | the amount is lower than (slots-)required |                            
| 62984  | 09:20:00 | 1      | 2             | 0      | 1         | both conditions above match here          |

So we get the two times 8:20 and 8:40 as free appointment times (or available processes) for booking