# Documentation

This folder includes some detailed documentation fragments for the application:

* [Software architecture and infrastructure considerations](architecture.md)
* [Entity relationship diagram](erm.md)
* [Database table relationship diagram](db-erm.md)
* [Lifecycle of the entity "process"](process_lifecycle.md)