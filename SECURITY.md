# Security

For sensitive content, the Vulnerability Disclosure Policy of Berlin.de can be used:

https://www.berlin.de/.well-known/security.txt

Please reference this repository when sending a message.